# FROM volkg/gpu-py3-jupy:ml-lab-tf-1.14.0
FROM volkg/gpu-py3-jupy:base-tf-latest

LABEL maintainer="Volker Gabler <v.gabler@tum.de>"

ARG U_ID
ARG G_ID

# add custom installations
USER root
COPY requirements.txt /tf/requirements.txt
WORKDIR /tf
RUN pip install -r requirements.txt
RUN rm /tf/requirements.txt

# create local user with proper IDs
RUN mkdir -p /etc/ssl/jupyter
RUN usermod -u ${U_ID} coder
RUN groupadd -g ${G_ID} g_coder
RUN usermod -g ${G_ID} coder
RUN chown -R ${U_ID}:${G_ID} /home/coder

USER coder
WORKDIR /home/coder
CMD ["jupyter", "notebook"]
#CMD ["jupyter", "lab"]

