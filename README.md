# Go Docker

Create your own customized programming python environment. For said purposes some base images have been set up, as to be looked up in [base_img](./base_img) or [ml_base_img](./ml_base_img) which contain a variety of widley used machine learning python-libraries if interested.

### Preparation

In order to build / run the commands in this repo, you need to be a member of the so called docker group, which you can check as shown here:
![docker group check](./media/id.png)

In case the 999(docker) is not listed, please contact admin@lsr.ei.tum.de


__Note: in order to use the local directory please run the following command before following the steps below:__

```sh 
lsr_create_local_workspace 
logging will be done in home directory at /home/gabler/.log/create_local_workspace.
[10:43:55->INFO]lsr_main.init_run:	==> Starting create_local_workspace Process
[10:43:55->INFO]client_operations.create_local_ws:	created local workspace /media/local_data/gabler
[10:43:55->INFO]client_operations.create_local_ws:	created soft link from /media/local_data/gabler to /home/gabler/local_gpuEpyc.
[10:43:55->INFO]lsr_main.finalize:	<== Finished create_local_workspace process
```

Furhter keep in mind that this step is __host-specific__, i.e. if you have run this on e.g. gpuEpyc, the procedure needs to be recalled on gpuTR.

## Short instructions

This image allows you to customize the python environment on our cluster PCs, there are two ways for you to follow:

1. I only need some python packages, on top of a cuda / tensorflow environment:
   Thou shalt follow the steps "Use this image"
2. I am an advanced user and I need a very customized environment:
   Thou shalt do your job and take a look at "Modify an image to your needs"

## Use this image

Before you start working on some image, check which version is of interest for you. Currently supported 

* TF 1.14, TF 1.15
* tf latest --> tracks current tensorflow image on dockerhub (with some delay of course)

Please recheck that you have followed the preparation steps!
The image will set up a virtual python docker container that allows GPU Cuda Tensorflow support as well as a jupyter (notebook/lab) environment.
The script ``lsr_gpu_docker`` has been added to our cluster PCs to build, run and modify the very basic settigns of this image.
Before proceeding with the actual build/run procedure outlined below, you may consider adjusting the image to your needs:

- if you need special packages / versions of packages, add these packages to the ```requirements.txt``` file. The image will install the packages.
- if you prefer working with jupyter notebook rather than jupyter lab, adjust the images as needed (add or remove the lab flag to the tags as shown in the Dockerfile and adjust the last line CMD)

Given this, the procedure to start and update your virtual python environment can be printed out by calling 

```sh 
lsr_gpu_docker
```

or in a step by step manner, by first calling the build

![Build docker image](./media/build.png)

and then running it

![Run docker container](./media/run.png)

when run, the printed link can directly accessed and will result in a warning that can be simply accepted:

![SSL warning](./media/Risk_accept.png)

which then will forward you to the login page, that requires you to copy the Security Token from the terminal output:

```
  Or copy and paste one of these URLs:
        https://(01a56afdb102 or 127.0.0.1):8888/?token=__c488dfd96622286827e3dd6a5c832a9c5b78be125099cd5f__
```

to the login screen. You can set a personal password, such that you can login without this token for the next connection as whon below:

![Set Password using a token](./media/EnterToken.png)


which then needs to be copied into the browser interface analogously.
The default view should then look similar to:

![Jupyter default view](./media/jupyter_nb_insight.png) ![Jupyter Lab default view](./media/jupyter_insight.png)

where

- data holds the network Documents directory,
- the local directory links to the __local__ SSD of the dedicated machine, if there is any available 
- tensorflow-tutorials serve the basic checks from tensorflow to test the CUDA backend
 

## Mofidy your image to your needs

__Please do not follow this path, if you do not know what you are doing. Dangling and/or  unnecessary ocker images will reduce space and power of cluster PCs__

In case the methods above are too limited for your needs, you can create your own docker image.
It is recommended to use the use the base images outlined in this repo and thus only adjust the main Dockerfile in this repository and proceed with the default build/run procedure.
For this purpose the build and run scripts for the images in this repository can be used as reference and start
In case you follow this path, you should understand the script and dockerfiles without further explanations as there is no further support provided.

## Sacred integration

You can further use a multi-container setup in order to enable 

- [sacred](https://github.com/IDSIA/sacred) 
- [omniboard](https://vivekratnavel.github.io/omniboard) web-interface

that allows to keep track of your current ML-algorithms perfomance

Due to the current runtime being not supported in docker-compose [see here](https://github.com/NVIDIA/nvidia-docker/wiki/Installation-(Native-GPU-Support)), the current workaround spanws the jupyter container manually. Hopefully fixed in a later relaese.
In contrast to the image above, you have to use the docker-compose syntax, which is wrapped in [gpu_docker_compose](./gpu_docker_compose).
This will write all variables in need to a ``.env `` file. In short, simply run:

``sh
./gpu_docker_compose up -d
``

which will outline the dedicated links to jupyter and omniboard:

![docker group check](./media/docker_compose_up.png)

This will instantiate a sacred http and a jupyter http__s__ instance, where you need to enter the token as mentioned on top.

