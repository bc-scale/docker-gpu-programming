# Extension of cuda base image

Dockarized container for extending base image with:
 
- well known machine learning modules ( see requirements.txt ) 
- in addition to installing [sacred](https://github.com/IDSIA/sacred), this image offers an additional port to an [omniboard](https://vivekratnavel.github.io/omniboard) web-interface, that allows to keep track of your current ML-algorithms perfomance

## Running this container setup



