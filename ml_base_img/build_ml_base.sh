#!/bin/bash
# admin script -> not for default users
# requests in base image can be add to
JUPY_DIR="$(pwd)/.jupyter"

function build_version_image(){
	if [ -z ${1} ]; then VERSION="latest"; else VERSION="${1}";fi
	echo "FROM volkg/gpu-py3-jupy:base-tf-${VERSION}" | tee  Dockerfile
	cat Dockerfile.template >> Dockerfile
	echo 'CMD ["jupyter", "notebook"]' >> Dockerfile
	docker build -t volkg/gpu-py3-jupy:ml-tf-${VERSION} .
	docker push volkg/gpu-py3-jupy:ml-tf-${VERSION}

 	echo "FROM volkg/gpu-py3-jupy:base-lab-tf-${VERSION}" | tee  Dockerfile
 	cat Dockerfile.template >> Dockerfile
 	echo 'CMD ["jupyter", "lab"]' >> Dockerfile
 	docker build -t volkg/gpu-py3-jupy:ml-lab-tf-${VERSION} .
 	docker push volkg/gpu-py3-jupy:ml-lab-tf-${VERSION}
}

docker login
build_version_image "1.14.0"
build_version_image "1.15.2"
build_version_image "latest"
